
from epicduels.content import game_state as GS

# FREEZING CHAMBER BOARD COORDINATES
FREEZING_CHAMBER = {'type': GS.FREEZING_CHAMBER,
                    'name': 'Freezing Chamber',
                    'states': {
                        GS.OBSTACLE: [{'x': 0, 'y': 0},
                                      {'x': 1, 'y': 0},
                                      {'x': 2, 'y': 0},
                                      {'x': 0, 'y': 1},
                                      {'x': 1, 'y': 1},
                                      {'x': 0, 'y': 6},
                                      {'x': 1, 'y': 6},
                                      {'x': 2, 'y': 6},
                                      {'x': 0, 'y': 5},
                                      {'x': 1, 'y': 5},
                                      {'x': 8, 'y': 0},
                                      {'x': 9, 'y': 0},
                                      {'x': 8, 'y': 6},
                                      {'x': 9, 'y': 6}]}}

# GEONOSIS BOARD COORDINATES
GEONOSIS = {'type': GS.GEONOSIS,
            'name': 'Geonosis',
            'states': {
                GS.OBSTACLE: [{'x': 0, 'y': 6},
                              {'x': 1, 'y': 6},
                              {'x': 2, 'y': 6},
                              {'x': 3, 'y': 6},
                              {'x': 6, 'y': 6},
                              {'x': 7, 'y': 6},
                              {'x': 8, 'y': 6},
                              {'x': 9, 'y': 6},
                              {'x': 9, 'y': 5},
                              {'x': 5, 'y': 1},
                              {'x': 6, 'y': 1},
                              {'x': 7, 'y': 1},
                              {'x': 7, 'y': 2}]}}

# KAMINO BOARD COORDINATES
KAMINO = {'type': GS.KAMINO,
          'name': 'Kamino',
          'states': {
              GS.OBSTACLE: [{'x': 3, 'y': 3},
                            {'x': 1, 'y': 4},
                            {'x': 2, 'y': 4},
                            {'x': 3, 'y': 4},
                            {'x': 2, 'y': 5},
                            {'x': 3, 'y': 5}],
              GS.HOLE:     [{'x': 6, 'y': 0},
                            {'x': 7, 'y': 0},
                            {'x': 8, 'y': 0},
                            {'x': 9, 'y': 0},
                            {'x': 6, 'y': 1},
                            {'x': 7, 'y': 1},
                            {'x': 8, 'y': 1},
                            {'x': 6, 'y': 6},
                            {'x': 7, 'y': 6},
                            {'x': 8, 'y': 6},
                            {'x': 9, 'y': 6},
                            {'x': 6, 'y': 5},
                            {'x': 7, 'y': 5},
                            {'x': 8, 'y': 5}]}}

# THRONE ROOM BOARD COORDINATES
THRONE_ROOM = {'type': GS.THRONE_ROOM,
               'name': 'Throne Room',
               'states': {
                   GS.OBSTACLE: [{'x': 0, 'y': 0},
                                 {'x': 1, 'y': 0},
                                 {'x': 2, 'y': 0},
                                 {'x': 3, 'y': 0},
                                 {'x': 4, 'y': 0},
                                 {'x': 5, 'y': 0},
                                 {'x': 0, 'y': 1},
                                 {'x': 1, 'y': 1},
                                 {'x': 0, 'y': 6},
                                 {'x': 1, 'y': 6},
                                 {'x': 2, 'y': 6},
                                 {'x': 3, 'y': 6},
                                 {'x': 4, 'y': 6},
                                 {'x': 5, 'y': 6},
                                 {'x': 0, 'y': 5},
                                 {'x': 1, 'y': 5}],
                   GS.HOLE: [{'x': 7, 'y': 1},
                             {'x': 7, 'y': 2},
                             {'x': 6, 'y': 2},
                             {'x': 9, 'y': 1},
                             {'x': 9, 'y': 2},
                             {'x': 6, 'y': 4},
                             {'x': 7, 'y': 4},
                             {'x': 7, 'y': 5},
                             {'x': 9, 'y': 4},
                             {'x': 9, 'y': 5}]}}

BOARDS = {
    GS.FREEZING_CHAMBER: FREEZING_CHAMBER,
    GS.GEONOSIS: GEONOSIS,
    GS.KAMINO: KAMINO,
    GS.THRONE_ROOM: THRONE_ROOM
}
