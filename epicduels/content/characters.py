
from epicduels.content import game_state as CT
from epicduels.content import decks

# Light is_main Characters
ANAKIN_SKYWALKER = {'name': 'Anakin Skywalker',
                    'hp': 18,
                    'is_main': True,
                    'max_hp': 18,
                    'type': CT.ANAKIN_SKYWALKER,
                    'state': CT.LIGHT,
                    'pos': None,
                    'is_range': False,
                    'deck': decks.ANAKIN_SKYWALKER_DECK}

HAN_SOLO = {'name': 'Han Solo',
            'hp': 13,
            'is_main': True,
            'max_hp': 13,
            'type': CT.HAN_SOLO,
            'state': CT.LIGHT,
            'pos': None,
            'is_range': True,
            'deck': decks.HAN_SOLO_DECK}

LUKE_SKYWALKER = {'name': 'Luke Skywalker',
                  'hp': 17, 'is_main': True,
                  'max_hp': 17,
                  'type': CT.LUKE_SKYWALKER,
                  'state': CT.LIGHT,
                  'pos': None,
                  'is_range': False,
                  'deck': decks.LUKE_SKYWALKER_DECK}

MACE_WINDU = {'name': 'Mace Windu',
              'hp': 19,
              'is_main': True,
              'max_hp': 19,
              'type': CT.MACE_WINDU,
              'state': CT.LIGHT,
              'pos': None,
              'is_range': False,
              'deck': decks.MACE_WINDU_DECK}

OBI_WAN = {'name': 'Obi-Wan Kenobi',
           'hp': 18,
           'is_main': True,
           'max_hp': 18,
           'type': CT.OBI_WAN,
           'state': CT.LIGHT,
           'pos': None,
           'is_range': False,
           'deck': decks.OBI_WAN_KENOBI_DECK}

YODA = {'name': 'Yoda',
        'hp': 15,
        'is_main': True,
        'max_hp': 15,
        'type': CT.YODA,
        'state': CT.LIGHT,
        'pos': None,
        'is_range': False,
        'deck': decks.YODA_DECK}

# minor Characters
CHEWBACCA = {'name': 'Chewbacca',
             'hp': 15,
             'is_main': False,
             'max_hp': 15,
             'type': CT.CHEWBACCA,
             'state': CT.LIGHT,
             'pos': None,
             'is_range': True,
             'deck': decks.CHEWBACCA_DECK}

LEIA_SKYWALKER = {'name': 'Leia Skywalker',
                  'hp': 10,
                  'is_main': False,
                  'max_hp': 10,
                  'type': CT.LEIA_SKYWALKER,
                  'state': CT.LIGHT,
                  'pos': None,
                  'is_range': True,
                  'deck': decks.LEIA_SKYWALKER_DECK}

PADME_AMIDALA = {'name': 'Padme Amidala',
                 'hp': 10,
                 'is_main': False,
                 'max_hp': 10,
                 'type': CT.PADME_AMIDALA,
                 'state': CT.LIGHT,
                 'pos': None,
                 'is_range': True,
                 'deck': decks.PADME_AMIDALA_DECK}

TROOPER = {'name': 'Trooper',
           'hp': 4,
           'is_main': False,
           'max_hp': 4,
           'type': CT.TROOPER,
           'state': CT.LIGHT,
           'pos': None,
           'is_range': True,
           'deck': decks.TROOPER_DECK}

# Dark is_main Characters
BOBA_FETT = {'name': 'Boba Fett',
             'hp': 14,
             'is_main': True,
             'max_hp': 14,
             'type': CT.BOBA_FETT,
             'state': CT.DARK,
             'pos': None,
             'is_range': True,
             'deck': decks.BOBA_FETT_DECK}

COUNT_DOOKU = {'name': 'Count Dooku',
               'hp': 18,
               'is_main': True,
               'max_hp': 18,
               'type': CT.COUNT_DOOKU,
               'state': CT.DARK,
               'pos': None,
               'is_range': False,
               'deck': decks.COUNT_DOOKU_DECK}

DARTH_MAUL = {'name': 'Darth Maul',
              'hp': 18,
              'is_main': True,
              'max_hp': 18,
              'type': CT.DARTH_MAUL,
              'state': CT.DARK,
              'pos': None,
              'is_range': False,
              'deck': decks.DARTH_MAUL_DECK}

DARTH_VADER = {'name': 'Darth Vader',
               'hp': 20,
               'is_main': True,
               'max_hp': 20,
               'type': CT.DARTH_VADER,
               'state': CT.DARK,
               'pos': None,
               'is_range': False,
               'deck': decks.DARTH_VADER_DECK}

EMPEROR_PALPATINE = {'name': 'Emperor Palpatine',
                     'hp': 13,
                     'is_main': True,
                     'max_hp': 13,
                     'type': CT.EMPEROR_PALPATINE,
                     'state': CT.DARK,
                     'pos': None,
                     'is_range': False,
                     'deck': decks.EMPEROR_PALPATINE_DECK}

JANGO_FETT = {'name': 'Jango Fett',
              'hp': 15,
              'is_main': True,
              'max_hp': 15,
              'type': CT.JANGO_FETT,
              'state': CT.DARK,
              'pos': None,
              'is_range': True,
              'deck': decks.JANGO_FETT_DECK}

# minor Characters
GREEDO = {'name': 'Greedo',
          'hp': 7,
          'is_main': False,
          'max_hp': 7,
          'type': CT.GREEDO,
          'state': CT.DARK,
          'pos': None,
          'is_range': True,
          'deck': decks.GREEDO_DECK}

ZAM_WESELL = {'name': 'Zam Wesell',
              'hp': 10,
              'is_main': False,
              'max_hp': 10,
              'type': CT.ZAM_WESELL,
              'state': CT.DARK,
              'pos': None,
              'is_range': True,
              'deck': decks.ZAM_WESELL_DECK}

BATTLE_DROID = {'name': 'Battle Droid',
                'hp': 3,
                'is_main': False,
                'max_hp': 3,
                'type': CT.BATTLE_DROID,
                'state': CT.DARK,
                'pos': None,
                'is_range': True,
                'deck': decks.TROOPER_DECK}

CRIMSON_GUARD = {'name': 'Crimson Guard',
                 'hp': 5,
                 'is_main': False,
                 'max_hp': 5,
                 'type': CT.CRIMSON_GUARD,
                 'state': CT.DARK,
                 'pos': None,
                 'is_range': True,
                 'deck': decks.CRIMSON_GUARD_DECK}

SUPER_BATTLE_DROID = {'name': 'Super Battle Droid',
                      'hp': 5,
                      'is_main': False,
                      'max_hp': 5,
                      'type': CT.SUPER_BATTLE_DROID,
                      'state': CT.DARK,
                      'pos': None,
                      'is_range': True,
                      'deck': decks.SUPER_BATTLE_DROID_DECK}

CHARACTERS = {
    CT.ANAKIN_SKYWALKER: ANAKIN_SKYWALKER,
    CT.HAN_SOLO: HAN_SOLO,
    CT.LUKE_SKYWALKER: LUKE_SKYWALKER,
    CT.MACE_WINDU: MACE_WINDU,
    CT.OBI_WAN: OBI_WAN,
    CT.YODA: YODA,
    CT.BOBA_FETT: BOBA_FETT,
    CT.COUNT_DOOKU: COUNT_DOOKU,
    CT.DARTH_MAUL: DARTH_MAUL,
    CT.DARTH_VADER: DARTH_VADER,
    CT.EMPEROR_PALPATINE: EMPEROR_PALPATINE,
    CT.JANGO_FETT: JANGO_FETT,
    CT.CHEWBACCA: CHEWBACCA,
    CT.TROOPER: TROOPER,
    CT.LEIA_SKYWALKER: LEIA_SKYWALKER,
    CT.PADME_AMIDALA: PADME_AMIDALA,
    CT.BATTLE_DROID: BATTLE_DROID,
    CT.CRIMSON_GUARD: CRIMSON_GUARD,
    CT.GREEDO: GREEDO,
    CT.SUPER_BATTLE_DROID: SUPER_BATTLE_DROID,
    CT.ZAM_WESELL: ZAM_WESELL
}

SQUADS = {
    CT.ANAKIN_SKYWALKER: [ANAKIN_SKYWALKER, PADME_AMIDALA],
    CT.HAN_SOLO: [HAN_SOLO, CHEWBACCA],
    CT.LUKE_SKYWALKER: [LUKE_SKYWALKER, LEIA_SKYWALKER],
    CT.MACE_WINDU: [MACE_WINDU, TROOPER, TROOPER],
    CT.OBI_WAN: [OBI_WAN, TROOPER, TROOPER],
    CT.YODA: [YODA, TROOPER, TROOPER],
    CT.BOBA_FETT: [BOBA_FETT, GREEDO],
    CT.COUNT_DOOKU: [COUNT_DOOKU, SUPER_BATTLE_DROID, SUPER_BATTLE_DROID],
    CT.DARTH_MAUL: [DARTH_MAUL, BATTLE_DROID, BATTLE_DROID],
    CT.DARTH_VADER: [DARTH_VADER, TROOPER, TROOPER],
    CT.EMPEROR_PALPATINE: [EMPEROR_PALPATINE, CRIMSON_GUARD, CRIMSON_GUARD],
    CT.JANGO_FETT: [JANGO_FETT, ZAM_WESELL]
}
