
from epicduels.model.dice import Dice
from epicduels.rules import game_rules as GR


class Game(object):

    def __init__(self):
        self.num_players = 0
        self.players = []
        self.squads = []
        self.board = ''
        self.dice = Dice(5)

    def create(self):
        """
        sets up game
        """
        self.num_players = GR.choose_number_of_players()
        self.squads = GR.initialize_player_squads(self.num_players)
        self.board = GR.initialize_board()
        move = GR.move_character(self.board, 2, {'x': 1, 'y': 1})
        print move

    def start(self):
        pass

    def stop(self):
        pass

    def save(self):
        pass

    def quit(self):
        pass


if __name__ == '__main__':
    new_game = Game()
    new_game.create()
    print(new_game.squads)
    print(new_game.board)