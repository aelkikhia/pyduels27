
import random
from epicduels.content import game_state as GS


class Dice(object):

    def __init__(self, sides=5):
        self.sides = sides
        self.result = self.roll()

    def print_result(self):
        return GS.ROLLS[self.result]

    def roll(self):
        self.result = GS.ROLLS[random.randint(0, self.sides)]
        return self.result
