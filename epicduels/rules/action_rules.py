

def attack():
    """
    Attack opponent
    """
    pass


def minors_dead(squad):
    """
    is/are minor character(s) dead
    """
    hp = 0
    for char in squad['characters']:
        if not char['is_main']:
            hp += char['hp']
    return hp == 0


def can_act(squad):
    """
    can squad do action
    """
    return squad['actions'] > 0


def can_play_card(squad):
    """
    can squad play card
    """
    return can_act(squad) and has_hand(squad)


def choose_action():
    """
    choose squad action
    """
    list_available_actions()


def choose_card(cards, index):
    """
    squad choose card
    """
    return cards.pop(index)


def defense():
    """
    defend against attack
    """
    pick_defense_card()


def discard_card(squad, index):
    """
    discards card from hand
    """


def discard_heal(squad, index):
    """
    action discard card to heal
    """
    if minors_dead(squad) and has_minor_card(squad):
        discard_card(squad, index)
        for character in squad['characters']:
            if character['is_main']:
                character['hp'] += 1


def draw_card(squad):
    """
    draw card from deck
    """
    squad['hand'].append(squad['deck'].pop())


def has_hand(squad):
    """
    does squad have hand
    """
    return len(squad['hand']) > 0


#TODO: think of best way to account for different subcharacters
def has_minor_card(squad):
    """
    check hand for card belonging to minor character
    """
    if has_hand(squad):
        for character in squad['characters']:
            if character['is_main']:
                for card in squad['hand']:
                    if card['owner'] != character['type']:
                        return True
    return False


def has_main_card(squad):
    """
    check hand for card belonging to main character
    """
    if has_hand(squad):
        for character in squad['characters']:
            if character['is_main']:
                for card in squad['hand']:
                    if card['owner'] == character['type']:
                        return True
    return False


def get_main_cards_list(squad):
    """
    check hand for card belonging to main character
    """
    if has_hand(squad):
        for character in squad['characters']:
            if character['is_main']:
                for card in squad['hand']:
                    if card['owner'] == character['type']:
                        return True
    return False


def pick_defense_card():
    pass


def pick_attack_card():
    pass


def pick_special_card():
    pass


def has_defense_card():
    pass


def has_attack_card():
    pass


def has_special_card():
    pass


def can_discard_to_heal(squad):
    pass


def list_available_actions(squad):
    # if has_minor_card(squad) and
    # return actions
    pass




