

# card rules
def anger(squad1, squad2):
    """
    attack 8, discard every card in your hand, except one card.
    """

    pass


def all_to_easy():
    """
    If this card is not blocked, the attacked character receives 20 damage
    instead of 3.
    """
    pass


def assassination():
    """
Assassination  - After attacking, you may move Zam to an empty space.
"""
    pass


def athletic_surge():
    """
Athletic Surge  - After attacking, you may move Darth Maul up to 6 spaces.
"""
    pass


def battlemind():
    """
Battlemind  - The attack and defense values of this card are equal to the number of cards in your hand after this card is played."
"""
    pass


def blinding_surge():
    """
Blinding Surge  - After taking the attacker's damage, Darth Maul does 3 points of damage to the attacking character.
"""
    pass


def bowcaster():
    """
Bowcaster  - Search your draw pile for the Bowcaster Attack card. If it is in your draw pile, put it in your hand. Then shuffle your draw pile.
"""
    pass


def calm():
    """
    Move character 8 spaces. if no cards in hand, draw 5 cards.
    """
    pass


def children_of_the_force():
    """
    move main 6 spaces, then move secondary 6 spaces. draw 2 cards
    """
    pass


def choke():
    """
    Choose any secondary enemy character. That character receives 6 damage.
    """
    pass


def counter_attack():
    """
Counterattack  - Anakin receives no damage from the attack. Instead the attacker receives 1 damage.
"""
    pass


def dark_side_drain():
    """
Dark Side Drain  - If Vader does damage to a character with this card, Vader recovers the amount of damage done to the character."
"""
    pass


def deadly_aim():
    """
Deadly Aim  - Draw 2 cards
"""
    pass


def desperate_shot():
    """
Desperate Shot  - If you do not destroy the defending character with this card, destroy Greedo.
"""
    pass


def fire_up_the_jet_pack():
    """
    You may move Jango Fett to any empty space. Playing this card does not count as an action.
    """
    pass


def flame_thrower():
    """
    does 2 damage to all characters adjacent to Jango Fett.
    You may then move these characters up to 3 spaces each.
    """
    pass


def force_balance():
    """
    All players discard their hands. Each player draws 3 new cards.
    """
    pass


def force_control():
    """
Force Control  - After attacking, you may move all characters in play up to 3 spaces each.
"""
    pass


def force_drain():
    """
Force Drain  - Pick 2 cards at random from an opponent's hand. That player must discard those cards.
"""
    pass


def force_lift():
    """
Force Lift  - Turn any character adjacent to Yoda on its side. This character cannot move, attack, or defend. At any time, any player may discard 3 cards to stand this character up.
"""
    pass


def force_lightning():
    """
Force Lightning  - Choose any character. That character receives 3 damage. The player "controlling this character must discard a card at random.
"""
    pass


def force_push_dooku():
    """
Force Push  - Move any character adjacent to to any empty space. That character receives 1 damage.
"""
    pass


def force_push_yoda():
    """
Force Push  - Move any character adjacent to to any empty space. That character receives 3 damage.
"""
    pass


def force_quickness():
    """
Force Quickness  - Move Obi-Wan up to 8 spaces, then draw a card
"""
    pass


def force_rebound():
    """
Force Rebound  - Yoda receives no damage from the attack. Instead, the attacker receives damage equal to the attack number on the attacker's card.
"""
    pass


def force_strike():
    """
Force Strike  - Draw 1 card
"""
    pass


def future_foreseen():
    """
Future Foreseen  - Look at the top 4 cards of your draw pile. Put one in your hand and put the other 3 cards back on top of your draw pile in any order."
"""
    pass


def gain_power():
    """
Gain Power  - Draw 3 cards
"""
    pass


def gamblers_luck():
    """
Gambler's Luck  - If Han does damage with this card, choose an opponent to discard a card at random.
"""
    pass


def give_orders():
    """
Give Orders  - Move Dooku up to 4 spaces. Then move Super Battledroid 1 up to 4 spaces and move Super Battledroid 2 up to 4 spaces.
"""
    pass


def heroic_retreat():
    """
Heroic Retreat  - After attacking, you may move Han up to 5 spaces.
"""
    pass


def i_will_not_fight_you():
    """
I Will Not Fight You  - Choose an opponent. You and the chosen opponent reveal your hands. Both of you discard all cards with an attack value greater than 1.
"""
    pass


def insight():
    """
Insight  - Look at any opponent's hand. Then choose one card. Your opponent must discard the chosen card.
"""
    pass


def its_not_wise():
    """
Its Not Wise  - Move any character adjacent to Chewbacca up to 3 spaces. That character receives 3 damage.
"""
    pass


def jedi_attack():
    """
Jedi Attack  - After attacking, you may move Obi-wan up to 6 spaces.
"""
    pass


def jedi_block():
    """
Jedi Block  - Draw 1 card
"""
    pass


def jedi_mind_trick():
    """
Jedi Mind Trick  - Take any card from your discard pile and put that card in your hand.
"""
    pass


def justice():
    """
Justice  - If Leia has been destroyed, the attack value of this card is 10.
"""
    pass


def kyber_dart():
    """
Kyber Dart  - "After attacking, if you destroy the defending character, draw 3 cards
"""
    pass


def latent_force_ability():
    """
Latent Force Abilities  - Draw 1 card
"""
    pass


def let_go_of_your_hatred():
    """
Let Go Of Your Hatred  - Choose an opponent. That opponent chooses and discards 2 cards.
"""
    pass


def lukes_in_trouble():
    """
Luke's In Trouble  - If Leia is adjacent to Luke, Luke recovers 3 damage. If Luke has been destroyed, Leia recovers 3 damage.
"""
    pass


def martial_defense():
    """
Martial Defense  - Draw 1 card
"""
    pass


def masterful_fighting():
    """
Masterful Fighting  - Draw 1 card
"""
    pass


def missile_launch():
    pass


def meditation():
    """
Meditation  - The Emperor recovers up to 4 damage. Choose an opponent. That opponent cannot draw cards during his/her next turn.
"""
    pass


def never_tell_me_the_odds():
    """
Never Tell Me The Odds  - Han does 2 damage to all opponents characters Han can attack. Then you may shuffle your discard pile into your draw pile.
"""
    pass


def precise_shot():
    """
Precise Shot  - After attacking, you may discard a card to draw a card.
"""
    pass


def protection():
    """
Protection  - If Anakin is alive, Padme recovers 4 damage. If Anakin has been destroyed, Padme recovers 2 damage.
"""
    pass


def rocket_retreat():
    """
Rocket Retreat  - "After attacking, you may move Fett to an empty space."
"""
    pass


def royal_command():
    """
Royal Command  - Exchange spaces between the Emperor and any Crimson Guard.
"""
    pass


def serenity():
    """
Serenity  - Draw 1 card
"""
    pass


def shot_on_the_run():
    """
Shot On The Run  - After attacking, you may move Padme up to 6 spaces.
"""
    pass


def sith_speed():
    """
Sith Speed  - Playing this card does not count as an action.
"""
    pass


def sniper_shot():
    """
Sniper Shot  - If this card is not blocked, Zam does 6 damage instead of 3.
"""
    pass


def super_sith_speed():
    """
Super Sith Speed  - Playing this card does not count as an action.
"""
    pass


def sudden_arrival():
    """
Sudden Arrival  - Move Greedo adjacent to any character. Playing this card does not count as an action.
"""
    pass


def taunting():
    """
Taunting  - Draw 1 card
"""
    pass


def thermal_detonator():
    """
Thermal Detonator  - Thermal Detonator does 4 damage to any one character Boba Fett can attack. All character adjacent to that character also receive 4 damage.
"""
    pass


def throw_debris():
    """
Throw Debris  - Choose any character. That character receives 4 damage.
"""
    pass


def whirlwind():
    """
Whirlwind  - Mace does 4 damage to all opponents characters he can attack.
"""
    pass


def wisdom():
    """
Wisdom  - You may move Mace up to 5 Spaces, then draw a card.
"""
    pass


def wookie_healing():
    """
Wookie Healing  - Chewbacca recovers up to 3 damage. The you may move Chewbacca up to 5 spaces.
"""
    pass


def wookie_instincts():
    """
Wookie Instincts  - Search your draw pile for the Bowcaster Attack card. If it is in your draw pile, put it in your hand. Then shuffle your draw pile.
"""
    pass


def wrath_anakin():
    """
Wrath Anakin  - You may move Anakin adjacent to any minor Character. That character receives 7 damage.
"""
    pass


def wrath_vader():
    """
Wrath Vader  - Choose an opponent. All of that opponent's characters receive 2 damage.
"""
    pass


def wrist_cable():
    """
Wrist Cable  - Wrist Cable does 2 damage to any one character Fett can attack. The player controlling the attacked character gets 1 less action on his/her next turn
"""
    pass


def you_will_die():
    """
You Will Die  - Choose an opponent. That opponent must discard his/her entire hand.
"""
    pass


def your_skills_are_not_complete():
    """
Your Skills Are Not Complete  - Choose any opponent. That opponent must reveal his/her hand and discard all special cards.
"""
    pass
