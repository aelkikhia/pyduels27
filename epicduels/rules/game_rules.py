
from random import shuffle

from epicduels.content import game_state as GS
from epicduels.rules import action_rules as AR
from epicduels.rules import board_rules as BR
from epicduels.model.menu import menu_loader
from epicduels.content import menus
from epicduels.initializers import initializer as init


def is_game_over(squads):
    """
    returns true if all main dark or light side characters are dead
    """
    return (sum([Character.hp for Character in characters if Character.is_main
            and Character.state == GS.LIGHT] for characters in squads) == 0) \
        or (sum([Character.hp for Character in characters if Character.is_main
            and Character.state == GS.DARK] for characters in squads) == 0)


def play_order():
    pass


def game_turn(game):
    """
    loop through every players turn
    """
    for squad in game.squads:

        #roll game dice
        game.dice.roll()

        #move squad members
        move_squad(game)

        #use actions
        use_actions(game)


def move_squad(game):
    for character in game.squad:
        move_character(game)


def move_character(board, moves, char_pos):
    """
    move character from current position to new position
    """
    move_menu = menus.MOVE_MENU
    move_menu['choices'] = BR.find_moves(board, moves, char_pos)

    menu = menu_loader(move_menu)
    choice = menu.load_menu()
    return choice


def use_actions(game):
    """
    calls choose action then uses returned action type as key to dictionary
    of action methods
    """
    # if game
    # action = AR.choose_action()
    pass


def choose_number_of_players(choice=None):
    if not choice:
        menu = menu_loader(menus.CHOOSE_NUM_PLAYERS)
        return menu.load_menu()
    return choice


def choose_player_sides(choice=None):
    if not choice:
        menu = menu_loader(menus.CHOOSE_PLAYER_SIDE)
        return menu.load_menu()
    return choice


def choose_players_squads(choice=None):
    if not choice:
        menu = menu_loader(menus.CHOOSE_SQUAD)
        return menu.load_enum_menu()
    return choice


def choose_game_board(choice=None):
    if not choice:
        menu = menu_loader(menus.CHOOSE_BOARD)
        choice = menu.load_enum_menu()
    return choice


def initialize_player_squads(num_players):
    squads = []
    for player_number in range(0, num_players+1):
        squads.append(init.setup_squad(player_number,
                                       choose_players_squads(),
                                       choose_player_sides()))
    return squads


def initialize_board(board_type=None):
    return init.board_initializer(choose_game_board(board_type))


def place_squad_on_board():
    pass


def deal_squad_hands(squads, hand_size=4):
    """
    Deals out hand to each squad
    Hand defaults to 4 to match an initial game start
    :param squads: (Dict)
    :param hand_size: (Int)
    :return:
    """
    for squad in squads:
        for x in range(0, hand_size):
            AR.draw_card(squad)


def shuffle_discard_into_deck(squad):
    """
    Inserts the discard list into the deck list and shuffles
    returns the squad dictionary
    :param squad:
    :return: squad (Dict)
    """
    squad['deck'].extend(squad['discard'])
    squad['discard'] = []
    squad['deck'] = shuffle(squad['deck'])
    return squad


